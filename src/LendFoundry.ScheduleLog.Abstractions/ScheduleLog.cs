using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Loans;
using System;

namespace LendFoundry.ScheduleLog
{
    public class ScheduleLog : Aggregate, IScheduleLog
    {
        public string LoanReferenceNumber { get; set; }

        public TimeBucket OperationTime { get; set; }

        public OperationType OperationType { get; set; }

        public DateTimeOffset AnniversaryDate { get; set; }

        public DateTimeOffset DueDate { get; set; }

        public DateTimeOffset GraceDate { get; set; }

        public DateTimeOffset PaymentDate { get; set; }

        public string PaymentId { get; set; }

        public PaymentMethod PaymentMethod { get; set; }

        public InstallmentType Type { get; set; }

        public InstallmentStatus Status { get; set; }

        public IPaydown Expected { get; set; }

        public IPaydown Actual { get; set; }

        public double OpeningBalance { get; set; }

        public double InterestBalance { get; set; }

        public double EndingBalance { get; set; }
    }
}