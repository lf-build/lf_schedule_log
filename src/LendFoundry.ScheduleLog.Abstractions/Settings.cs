﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace LendFoundry.ScheduleLog
{
    public class Settings
    {
        private const string Prefix = "SCHEDULE_LOG";

        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");

        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION", "configuration");

        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");

        public static DatabaseSettings Mongo { get; } = new DatabaseSettings($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", "schedule-log");

        public static string ServiceName => Environment.GetEnvironmentVariable($"{Prefix}_TOKEN_ISSUER") ?? "schedule-log";
    }
}