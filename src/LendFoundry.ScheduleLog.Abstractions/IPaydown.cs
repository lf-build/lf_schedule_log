﻿namespace LendFoundry.ScheduleLog
{
    public interface IPaydown
    {
        double Amount { get; }
        double Interest { get; }
        double Principal { get; }
    }
}