using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Loans;
using System;

namespace LendFoundry.ScheduleLog
{
    public interface IScheduleLog : IAggregate
    {
        string LoanReferenceNumber { get; set; }

        TimeBucket OperationTime { get; set; }

        OperationType OperationType { get; set; }

        DateTimeOffset AnniversaryDate { get; set; }

        DateTimeOffset DueDate { get; set; }

        DateTimeOffset GraceDate { get; set; }

        DateTimeOffset PaymentDate { get; set; }

        string PaymentId { get; set; }

        PaymentMethod PaymentMethod { get; set; }

        InstallmentType Type { get; set; }

        InstallmentStatus Status { get; set; }

        IPaydown Expected { get; set; }

        IPaydown Actual { get; set; }

        double OpeningBalance { get; set; }

        double InterestBalance { get; set; }

        double EndingBalance { get; set; }
    }
}