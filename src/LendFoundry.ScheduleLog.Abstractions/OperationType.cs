﻿namespace LendFoundry.ScheduleLog
{
    public enum OperationType
    {
        Undefined = 0,

        PaymentCancelled = 1,

        PaymentReceived = 2,

        PaymentUpdated = 3
    }
}