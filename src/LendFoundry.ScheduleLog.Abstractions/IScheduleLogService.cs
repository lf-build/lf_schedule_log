using LendFoundry.Loans;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.ScheduleLog
{
    public interface IScheduleLogService
    {
        Task<IEnumerable<IScheduleLog>> GetAllAsync(string loanReferenceNumber);

        Task<IEnumerable<IScheduleLog>> GetAllByPaymentIdAsync(string loanReferenceNumber, string paymentId);

        Task<IEnumerable<IScheduleLog>> GetAllByPaymentMethodAsync(string loanReferenceNumber, PaymentMethod paymentMethod);

        Task<IScheduleLog> GetLastByPaymentIdAsync(string loanReferenceNumber, string paymentId);
    }
}