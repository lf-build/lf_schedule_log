using LendFoundry.Foundation.Persistence;
using LendFoundry.Loans;
using System.Collections.Generic;

namespace LendFoundry.ScheduleLog
{
    public interface IScheduleLogRepository : IRepository<IScheduleLog>
    {
        IEnumerable<IScheduleLog> GetAll(string loanReferenceNumber);

        IEnumerable<IScheduleLog> GetAllByPaymentId(string loanReferenceNumber, string paymentId);

        IEnumerable<IScheduleLog> GetAllByPaymentMethod(string loanReferenceNumber, PaymentMethod paymentMethod);

        IScheduleLog GetLastByPaymentId(string loanReferenceNumber, string paymentId);
    }
}