using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Loans;

namespace LendFoundry.ScheduleLog
{
    public class ScheduleLogService : IScheduleLogService
    {
        public Task<IEnumerable<IScheduleLog>> GetAllAsync(string loanReferenceNumber)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<IScheduleLog>> GetAllByPaymentIdAsync(string loanReferenceNumber, string paymentId)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<IScheduleLog>> GetAllByPaymentMethodAsync(string loanReferenceNumber, PaymentMethod paymentMethod)
        {
            throw new NotImplementedException();
        }

        public Task<IScheduleLog> GetLastByPaymentIdAsync(string loanReferenceNumber, string paymentId)
        {
            throw new NotImplementedException();
        }
    }
}