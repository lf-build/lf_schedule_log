using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LendFoundry.Loans;

namespace LendFoundry.ScheduleLog.Persistence
{
    public class ScheduleLogRepository : IScheduleLogRepository
    {
        public void Add(IScheduleLog item)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<IScheduleLog>> All(Expression<Func<IScheduleLog, bool>> query, int? skip = default(int?), int? quantity = default(int?))
        {
            throw new NotImplementedException();
        }

        public int Count(Expression<Func<IScheduleLog, bool>> query)
        {
            throw new NotImplementedException();
        }

        public Task<IScheduleLog> Get(string id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IScheduleLog> GetAll(string loanReferenceNumber)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IScheduleLog> GetAllByPaymentId(string loanReferenceNumber, string paymentId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IScheduleLog> GetAllByPaymentMethod(string loanReferenceNumber, PaymentMethod paymentMethod)
        {
            throw new NotImplementedException();
        }

        public IScheduleLog GetLastByPaymentId(string loanReferenceNumber, string paymentId)
        {
            throw new NotImplementedException();
        }

        public void Remove(IScheduleLog item)
        {
            throw new NotImplementedException();
        }

        public void Update(IScheduleLog item)
        {
            throw new NotImplementedException();
        }
    }
}