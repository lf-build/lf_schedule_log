using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Loans;
using Microsoft.AspNet.Mvc;
using System.Threading.Tasks;

namespace LendFoundry.ScheduleLog.Api.Controllers
{
    [Route("/")]
    public class ApiController : ExtendedController
    {
        public ApiController(IScheduleLogService service, ILogger logger): base (logger)
        {
            Service = service;
        }

        private IScheduleLogService Service { get; }


        [HttpGet("/{loanReferenceNumber}")]
        public Task<IActionResult> GetAllAsync(string loanReferenceNumber)
        {
            return ExecuteAsync(async () => new HttpOkObjectResult(await Service.GetAllAsync(loanReferenceNumber)));
        }

        [HttpGet("/{loanReferenceNumber}/{paymentId}")]
        public Task<IActionResult> GetAllByPaymentId(string loanReferenceNumber, string paymentId)
        {
            return ExecuteAsync(async () => new HttpOkObjectResult(await Service.GetAllByPaymentIdAsync(loanReferenceNumber, paymentId)));
        }

        [HttpGet("/{loanReferenceNumber}/{paymentId}/last")]
        public Task<IActionResult> GetLastByPaymentId(string loanReferenceNumber, string paymentId)
        {
            return ExecuteAsync(async () => new HttpOkObjectResult(await Service.GetLastByPaymentIdAsync(loanReferenceNumber, paymentId)));
        }

        [HttpGet("/{loanReferenceNumber}/{paymentMethod}")]
        public Task<IActionResult> GetAllByPaymentMethod(string loanReferenceNumber, PaymentMethod paymentMethod)
        {
            return ExecuteAsync(async () => new HttpOkObjectResult(await Service.GetAllByPaymentMethodAsync(loanReferenceNumber, paymentMethod)));
        }
    }
}