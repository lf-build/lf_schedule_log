using LendFoundry.ScheduleLog.Persistence;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Abstractions;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Services.Handlebars;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.ScheduleLog.Api
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            // services
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantTime();
            services.AddConfigurationService<ScheduleLogConfiguration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.ServiceName);
            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);

            // interface implements
            services.AddTransient(p =>
            {
                var currentTokenReader = p.GetService<ITokenReader>();
                var staticTokenReader = new StaticTokenReader(currentTokenReader.Read());
                return p.GetService<IEventHubClientFactory>().Create(staticTokenReader);
            });
            services.AddTransient<IScheduleLogConfiguration>(p => p.GetService<IConfigurationService<ScheduleLogConfiguration>>().Get());
            services.AddSingleton<IMongoConfiguration>(p => new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database));
            services.AddTransient<IScheduleLogRepository, ScheduleLogRepository>();
            services.AddTransient<IScheduleLogService, ScheduleLogService>();
            services.AddTransient<IScheduleLogRepositoryFactory, ScheduleLogRepositoryFactory>();
            services.AddTransient<IScheduleLogServiceFactory, ScheduleLogServiceFactory>();
            services.AddTransient<IScheduleLogListener, ScheduleLogListener>();

            // aspnet mvc related
            services.AddMvc().AddLendFoundryJsonOptions((options) =>
            {
                options
                    .AddInterfaceConverter<IScheduleLog, ScheduleLog>()
                    .AddInterfaceConverter<IEventInfo, EventInfo>();
            });
            services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors(env);
            app.UseErrorHandling();
            app.UseRequestLogging();
            //app.UseScheduleLogListener();
            app.UseMvc();
            app.UseHandlebars();
        }
    }
}