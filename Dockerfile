FROM registry.lendfoundry.com/base:beta8

ADD ./src/LendFoundry.ScheduleLog.Abstractions /app/LendFoundry.ScheduleLog.Abstractions
WORKDIR /app/LendFoundry.ScheduleLog.Abstractions
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.ScheduleLog.Persistence /app/LendFoundry.ScheduleLog.Persistence
WORKDIR /app/LendFoundry.ScheduleLog.Persistence
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.ScheduleLog /app/LendFoundry.ScheduleLog
WORKDIR /app/LendFoundry.ScheduleLog
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.ScheduleLog.Api /app/LendFoundry.ScheduleLog.Api
WORKDIR /app/LendFoundry.ScheduleLog.Api
RUN eval "$CMD_RESTORE"
RUN dnu build

EXPOSE 5000
ENTRYPOINT dnx kestrel